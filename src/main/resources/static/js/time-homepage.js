let dashboardDate = document.getElementById("dashboard-date");
let date = new Date();

// console.log(`${date.getDate()}-${date.toLocaleString('default', { month: 'long' })}-${date.getFullYear()}`);
dashboardDate.innerHTML = `${date.getDate()}  ${date.toLocaleString('default', { month: 'long' })}  ${date.getFullYear()}`;