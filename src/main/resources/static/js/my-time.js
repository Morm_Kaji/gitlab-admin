
    // timer
    let hours = document.querySelector('.hours');
    let minutes = document.querySelector('.minutes');
    let seconds = document.querySelector('.seconds');
    let ampm = document.querySelector('.ampm');

    const twoDigit = (num) => {
    return num > 9 ? "" + num : "0" + num;
}

    const formatAMPM = (hours) => {
    let ampm = hours >= 12 ? 'PM' : 'AM';
    return ampm;
}

    let myTime = () => {
    const d = new Date();
    ampm.innerHTML = formatAMPM(d.getHours());
    seconds.innerHTML = twoDigit(d.getSeconds());
    minutes.innerHTML = twoDigit(d.getMinutes());
    hours.innerHTML = twoDigit(d.getHours());
}

    setInterval(myTime, 1000)
    // end-timer

    // current-date
    let currDate = document.querySelector('.curr-date');

    const dateFormat = () => {
    let date = new Date();
    let year = new Intl.DateTimeFormat('en', { year: 'numeric' }).format(date);
    let month = new Intl.DateTimeFormat('en', { month: 'short' }).format(date);
    let day = new Intl.DateTimeFormat('en', { day: '2-digit' }).format(date);

    // currDate.innerHTML = ${day} ${month} ${year};
}

    setInterval(dateFormat, 1000)
    // end-current-date

