==========< Welcome to HRD-EXAM >==========

    ==< This application have to three main user
    # Admin
    # Teachers
    # Students

    ==< LoginOption (login page)
    # When we run application , it will show
    # the login page as admin
    # Users can't login unless then complete the requirement fill

=====================< Home Page >===================== start

    =====================> Admin (admin home page) < start >

    => Calendar
    => Clock

    => all page have navbar
    # menu
    # admin profile photo
    # shortcut create teacher
    # Can logout
    # Can goto profile

    => Home page
    # get admin name from repository to show
    # get all number of data (student, teacher, class , generation) from repository to show them on cards
    # Can click on student card to goto student management
    # Can click on teacher card to goto teacher management
    # Can click on class card to goto class management
    # Can click on generation card to goto generation management
    # Can view and make changes in profile's information

    => Teacher Management
    # can create teacher without duplicate username
    # Show all teacher in table
    # Can search teacher by Fullname or username or email
    # Can view teacher profile
    # Can remove teacher
    # Can edit information teacher but can not view or change teacher password
    # Pagination


  => Student Management
  # Show all student in table that have in active generation
  # Can filter student by class
  # Can search teacher by Fullname or username or email
  # Can view student profile but can not view student password
  # Can remove student
  # Can edit information student but can not change student password or generation
  # Pagination

  => Class management
  # get all Class data from repository and show as cards
  # Can create new class
  # Can edit class name
  # Can remove Class
  # Pagination

  => Generation management
  # get all generation data from repository and show as cards
  # can create new generation
  # can edit generation but can not edit archived generation
  # can remove generation but can not remove archived or active generation
  # show total number of classwork and student by generation
  # show student data in table by generation but readonly

  => Profile
  # Show profile information
  # Can edit profile
  # can change password

