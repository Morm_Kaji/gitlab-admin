package com.kshrd.hrdexamadmindashboard.service;


import com.kshrd.hrdexamadmindashboard.model.*;
import com.kshrd.hrdexamadmindashboard.repository.AdminRepository;
import com.kshrd.hrdexamadmindashboard.utility.Paging;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.processing.FilerException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.UUID;

@Service
public class AdminServiceImp implements AdminService{

    private Path adminProfile;

    @Autowired
    public AdminServiceImp(@Value("${file.storage.server.path}")String path){
        adminProfile = Paths.get(path);
    }

    @Autowired
    private AdminRepository adminRepository;

    @Override
    public Boolean changePasswordAdmin(Admin admin, int adminId) {
        return adminRepository.changePasswordAdmin(admin,adminId);
    }

    @Override
    public Admin getAdminByUsername(String username) {
        return adminRepository.getAdminByUsername(username);
    }

    @Override
    public Admin getAdminById(int id) {
        return adminRepository.getAdminById(id);
    }

    @Override
    public Boolean updateAdminById(Admin admin, int adminId) {

        return  adminRepository.updateAdminById(admin, adminId );
    }

    //    Teachers Management

    @Override
    public void registerTeacher(Teacher teacher) {
         adminRepository.registerTeacher(teacher);
    }

    @Override
    public List<Teacher> getAllTeachers() {
        return adminRepository.getAllTeachers();
    }

    @Override
    public Teacher getTeacherById(String teacher) {
        return adminRepository.getTeachersById(teacher);
    }

    @Override
    public void updateTeacher(Teacher teacher, String id) {
         adminRepository.updateTeacher(teacher, id);
    }

    @Override
    public void deleteTeacherById(String teacherID) {
         adminRepository.deleteTeacherById(teacherID);
    }

    @Override
    public List<Teacher> fetchAllTeachers(Paging paging) {
        return adminRepository.fetchAllTeachers(paging);
    }

    @Override
    public int countAllTeacher() {
        return adminRepository.countAllTeacher();
    }

    @Override
    public List<Teacher> searchTeacherByIdOrName(String search, Paging paging) {
        return adminRepository.searchTeacherByIdOrName(search,paging);
    }

    @Override
    public int countSearchTeacher(String search) {
        return adminRepository.countSearchTeacher(search);
    }

//    Students Management

    @Override
    public List<Student> getAllStudents() {
        return adminRepository.getAllStudents();
    }

    @Override
    public List<Student> findStudentsByIdOrName(String search) { return adminRepository.findStudentsByIdOrName(search); }

    @Override
    public List<Student> findStudentsByIdOrNameWithGenID(String search, int generationId) {
        return adminRepository.findStudentsByIdOrNameWithGenID(search,generationId);
    }

    @Override
    public Student getStudentsById(String id) {
        return adminRepository.getStudentsById(id);
    }

    @Override
    public List<Student> getStudentsByClass(int classID) {
        return adminRepository.getStudentsByClass(classID);
    }

    @Override
    public List<Student> getStudentsByGenerationAndClass(int classID, int generationId) {
        return adminRepository.getStudentsByGenerationAndClass(classID,generationId);
    }

    @Override
    public List<Student> getStudentsByGeneration(int generationId) {
        return adminRepository.getStudentsByGeneration(generationId);
    }

    @Override
    public void updateStudent(Student student, String id) {
         adminRepository.updateStudent(student,id);
    }

    @Override
    public boolean checkTeacherUsername(String username) {
        return adminRepository.checkTeacherUsername(username);
    }

    @Override
    public List<Student> getAvailableClassByStudent(int genId) {
        return adminRepository.getAvailableClassByStudentAndGen(genId);
    }

    @Override
    public void deleteStudents(String studentId) {
         adminRepository.deleteStudents(studentId);
    }

    @Override
    public List<Student> fetchStudentsByGeneration(int generationId, Paging paging) {
        return adminRepository.fetchStudentsByGeneration(generationId, paging);
    }

    @Override
    public int countStudentsByGeneration(int generationId) {
        return adminRepository.countStudentsByGeneration(generationId);
    }

    @Override
    public List<Student> searchStudentsByIdOrNameWithGenID(String search, int generationId, Paging paging) {
        return adminRepository.searchStudentsByIdOrNameWithGenID(search,generationId,paging);
    }

    @Override
    public int countSearchStudents(String search, int generationId) {
        return adminRepository.countSearchStudents(search,generationId);
    }

    @Override
    public List<Student> searchStudentsByIdOrNameWithGenIdAndClassId(String search, int classId, int generationId, Paging paging) {
        return adminRepository.searchStudentsByIdOrNameWithGenIdAndClassId(search,classId,generationId,paging);
    }

    @Override
    public int countSearchStudentsByGenerationAndClass(String search, int classID, int generationId) {
        return adminRepository.countSearchStudentsByGenerationAndClass(search,classID,generationId);
    }

    @Override
    public List<Student> fetchStudentsByGenerationAndClass(int classID, int generationId, Paging paging) {
        return adminRepository.fetchStudentsByGenerationAndClass(classID,generationId,paging);
    }

    @Override
    public int countStudentsByGenerationAndClass(int classID, int generationId) {
        return adminRepository.countStudentsByGenerationAndClass(classID,generationId);
    }
//    Class And Generation

    @Override
    public void createClass(String classNames) {
         adminRepository.createClass(classNames);
    }

    @Override
    public List<StudentClass> getAllClass() {
        return adminRepository.getAllClass();
    }

    @Override
    public void updateClass(StudentClass classStudents, int id) {
         adminRepository.updateClass(classStudents, id);
    }

    @Override
    public void deleteClass(int id) {
         adminRepository.deleteClass(id);
    }

    @Override
    public void createGeneration(Generation generation) {
         adminRepository.createGeneration(generation.getGenerationName(), generation.getStatus(), generation.getType());
    }

    @Override
    public List<Generation> getAllGeneration() {
        return adminRepository.getAllGeneration();
    }

    @Override
    public Generation getGenerationByID(int generationId) {
        return adminRepository.getGenerationByID(generationId);
    }

    @Override
    public Generation getGenerationByStatus(int status) {
        return adminRepository.getGenerationByStatus(status);
    }

    @Override
    public List<Teacher> findTeacherByIdOrName(String search) {
        return adminRepository.findTeacherByIdOrName(search);
    }

    @Override
    public void updateGenerations(Generation generation, int id) {
         adminRepository.updateGenerations(generation, id);
    }

    @Override
    public void deleteGeneration(int id) {
         adminRepository.deleteGeneration(id);
    }

    @Override
    public String profile(MultipartFile file) {

        try{
            String filename = file.getOriginalFilename();
            if(filename.contains((".."))){
                System.out.println("Profile is null");
                return null;
            }
            String[] filepath = filename.split("\\.");
            String extension = filepath[1];
            filename = UUID.randomUUID() + "." + extension;
            Path targetLocations = adminProfile.resolve(filename);
            Files.copy(file.getInputStream(), targetLocations, StandardCopyOption.REPLACE_EXISTING);
            return filename;
        }catch(FilerException e){
            e.printStackTrace();
        }catch(IOException ex){
            ex.printStackTrace();
        }

        return null;
    }

    @Override
    public List<Exam> getAllExamByGenId(int genId) {
        return adminRepository.getAllExamByGenId(genId);
    }

    @Override
    public List<Generation> fetchAllGeneration(Paging paging) {
        return adminRepository.fetchAllGeneration(paging);
    }

    @Override
    public int countAllGeneration() {
        return adminRepository.countAllGeneration();
    }

    @Override
    public List<StudentClass> fetchAllClass(Paging paging) {
        return adminRepository.fetchAllClass(paging);
    }

    @Override
    public int countAllClass() {
        return adminRepository.countAllClass();
    }
}
