package com.kshrd.hrdexamadmindashboard.service;

import com.kshrd.hrdexamadmindashboard.model.Admin;
import com.kshrd.hrdexamadmindashboard.repository.AdminRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class MyUserDetailService implements UserDetailsService {

    @Autowired
    private AdminRepository adminRepository;


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        Admin admin = adminRepository.getAdminByUsername(username);

        if(admin != null) {
            return new CustomAdminDetail(admin);
        }else {
            throw new UsernameNotFoundException("User not found");
        }
    }
}
