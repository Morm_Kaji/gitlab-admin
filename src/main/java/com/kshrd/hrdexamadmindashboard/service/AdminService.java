package com.kshrd.hrdexamadmindashboard.service;

import com.kshrd.hrdexamadmindashboard.model.*;
import com.kshrd.hrdexamadmindashboard.utility.Paging;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface AdminService {

    //--------------admin-------------------------
    Admin getAdminByUsername(String username);

    Admin getAdminById(int id);

    Boolean updateAdminById(Admin admin, int adminId);

    Boolean changePasswordAdmin(Admin admin, int adminId);

    //----------------teacher---------------------
    void registerTeacher(Teacher teacher);

    List<Teacher> getAllTeachers();

    Teacher getTeacherById(String teacher);

    void updateTeacher(Teacher teacher, String id);

    void deleteTeacherById(String teacherID);

    List<Teacher> findTeacherByIdOrName(String search);

    //    ------------ with pagination-------------------
    List<Teacher> fetchAllTeachers(@Param("page") Paging paging);

    int countAllTeacher();

    List<Teacher> searchTeacherByIdOrName(String search, @Param("param") Paging paging);

    int countSearchTeacher(String search);
    boolean checkTeacherUsername(String username);
    //-----------------student----------------------
    List<Student> getAllStudents();

    List<Student> findStudentsByIdOrName(String search);

    List<Student> findStudentsByIdOrNameWithGenID(String search, int generationId);

    Student getStudentsById(String id);

    List<Student> getStudentsByClass(int classID);

    List<Student> getStudentsByGeneration(int generationId);

    List<Student> getStudentsByGenerationAndClass(int classID, int generationId);

    void updateStudent(Student student, String id);

    void deleteStudents(String studentId);

    List<Student> getAvailableClassByStudent(int genId);
    //    ------------ pagination-----------
    List<Student> fetchStudentsByGeneration(int generationId, Paging paging);

    int countStudentsByGeneration(int generationId);

    List<Student> searchStudentsByIdOrNameWithGenID(String search, int generationId, @Param("page") Paging paging);

    int countSearchStudents(String search, int generationId);

    List<Student> fetchStudentsByGenerationAndClass(int classID, int generationId, @Param("page") Paging paging);

    int countStudentsByGenerationAndClass(int classID, int generationId);

    List<Student> searchStudentsByIdOrNameWithGenIdAndClassId(String search, int classId, int generationId, @Param("page") Paging paging);

    int countSearchStudentsByGenerationAndClass(String search, int classID, int generationId);

    //------------------class-----------------------
    void createClass(String className);

    List<StudentClass> getAllClass();

    void updateClass(StudentClass classStudents, int id);

    void deleteClass(int id);

    //----------------- generation--------------------
    void createGeneration(Generation generation);

    List<Generation> getAllGeneration();

    Generation getGenerationByID(int generationId);

    Generation getGenerationByStatus(int status);

    void updateGenerations(Generation generations, int id);

    void deleteGeneration(int id);

    String profile(MultipartFile file) throws Exception;

    List<Exam> getAllExamByGenId(int genId);

    List<Generation> fetchAllGeneration(@Param("page") Paging paging);
    int countAllGeneration();
    List<StudentClass> fetchAllClass(@Param("page") Paging paging);
    int countAllClass();

}
