package com.kshrd.hrdexamadmindashboard.service;

import java.util.ArrayList;
import java.util.List;

public class Utility {



    private final List<String> genders = new ArrayList<>();

    public List<String> getGendersList() {
        genders.add("Male");
        genders.add("Female");
        return genders;
    }
}
