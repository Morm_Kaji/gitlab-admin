package com.kshrd.hrdexamadmindashboard.controller;

import com.kshrd.hrdexamadmindashboard.model.Admin;
import com.kshrd.hrdexamadmindashboard.model.Teacher;
import com.kshrd.hrdexamadmindashboard.service.AdminServiceImp;
import com.kshrd.hrdexamadmindashboard.service.Utility;
import com.kshrd.hrdexamadmindashboard.utility.Paging;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
//import org.modelmapper.internal.bytebuddy.utility.RandomString;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Controller
public class TeacherManagementController {

    @Value("${file.storage.client.path}")
//    @Value("${file.storage.server.path}")
    String serverPath ;

    String teacherIdForUpdate;

    @Autowired
    private AdminServiceImp adminServiceImp;

    @Autowired
    private PasswordEncoder passwordEncoder;

    Paging paging = new Paging();

    //========================================================================================//
    //                                                                                        //
    //========================================================================================//
    //                                                                                        //
    //                                                                                        //
    //                                  Get All Teacher                                       //
    //                                                                                        //
    //                                                                                        //
    //****************************************************************************************//

    @GetMapping("/dashboard/teacher/management")
    public String teacherManagement(Model model,@RequestParam(required = false) String page){
        Admin admin= adminServiceImp.getAdminById(999);
        model.addAttribute("Admin", admin);

        paging.setLimit(10);
        if(page==null){
            paging.setPage(1);
        }else {
            paging.setPage(Integer.parseInt(page));
        }
        paging.setTotalCount(adminServiceImp.countAllTeacher());
        System.out.println(paging.getTotalCount());
        List<Teacher> teacher = adminServiceImp.fetchAllTeachers(paging);
//        System.out.println("teacher search result : "+teacher);

        model.addAttribute("teachers", teacher);
        model.addAttribute("paging",paging);
        return "teacher-management";
    }

    //========================================================================================//
    //                                                                                        //
    //========================================================================================//
    //                                                                                        //
    //                                                                                        //
    //                                  search Teacher                                       //
    //                                                                                        //
    //                                                                                        //
    //****************************************************************************************//

    @GetMapping("/dashboard/teacher/management/")
    public String fetchAllTeacher(Model model,@RequestParam(required = false) String page,
                                @RequestParam(required = false) String search){
        Admin admin= adminServiceImp.getAdminById(999);
        model.addAttribute("Admin", admin);

        paging.setLimit(10);
        if(page==null){
            paging.setPage(1);
        }else {
            paging.setPage(Integer.parseInt(page));
        }
        if((search==null)||(search=="")){
            paging.setTotalCount(adminServiceImp.countAllTeacher());
            List<Teacher> teacher = adminServiceImp.fetchAllTeachers(paging);
            model.addAttribute("teachers", teacher);
//            System.out.println(paging.getTotalCount());
        }else {
            paging.setTotalCount(adminServiceImp.countSearchTeacher("%"+search+"%"));
            System.out.println("number of total row"+paging.getTotalCount());
            List<Teacher> teacher = adminServiceImp.searchTeacherByIdOrName("%"+search+"%",paging);
            model.addAttribute("teachers", teacher);
//            System.out.println("Param :"+ search);
        }

//        System.out.println("teacher search result : "+teacher);

        model.addAttribute("textSearch",search);
        model.addAttribute("paging",paging);
        return "teacher-management";
    }


    //========================================================================================//
    //                                      * Delete Teacher                                  //
    //========================================================================================//
    //          Delete by Change Status of teacher to 0                                       //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //****************************************************************************************//

    @GetMapping("/dashboard/teacher/management/{id}/delete")
    public String deleteTeacher(@PathVariable String id){

        adminServiceImp.deleteTeacherById(id);

        return "redirect:/dashboard/teacher/management";
    }

    //========================================================================================//
    //                                                                                        //
    //========================================================================================//
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //****************************************************************************************//
    @GetMapping("/dashboard/teacher/register")
    public String goRegister(Model model){
        Admin admin= adminServiceImp.getAdminById(999);
        Teacher teacher = new Teacher();

        model.addAttribute("Admin", admin);
        model.addAttribute("teacher",teacher);
        return "teacher_registration";
    }

    @PostMapping("/dashboard/teacher/register")
    public String registerTeacher(Model model, @ModelAttribute Teacher teacher, @RequestPart("file") MultipartFile file){
        Admin admin= adminServiceImp.getAdminById(999);


        String savedFileName;

        System.out.println("Teacher from html :"+teacher);
        Teacher registerTeacher = new Teacher();
        registerTeacher.setId(UUID.randomUUID().toString());
        registerTeacher.setFullName(teacher.fullName);
        registerTeacher.setUsername(teacher.username);
        registerTeacher.setGender(teacher.gender);
        registerTeacher.setEmail(teacher.email);
        registerTeacher.setPassword(passwordEncoder.encode(teacher.password));
        registerTeacher.setStatus(1);

        boolean hasTeacherUsername = adminServiceImp.checkTeacherUsername(registerTeacher.getUsername());
        if(hasTeacherUsername){
            model.addAttribute("Admin", admin);
            model.addAttribute("teacher",registerTeacher);
            return "teacher_registration";
        }else {
            if(!file.isEmpty()){
                String orginalFileName =  file.getOriginalFilename();
                savedFileName = UUID.randomUUID().toString()+"-"+orginalFileName;
                try{
                    Files.copy(file.getInputStream(), Paths.get(serverPath,savedFileName));
                    System.out.println(Paths.get(serverPath,savedFileName));

                    registerTeacher.setProfileImg(Paths.get(serverPath,savedFileName).toString());

                }  catch (IOException e){
                    e.printStackTrace();
                }
            }
            adminServiceImp.registerTeacher(registerTeacher);
            return "redirect:/dashboard/teacher/management";
        }

//        System.out.println("Register teacher: "+registerTeacher);
    }


    //========================================================================================//
    //                                  Update teacher                                        //
    //========================================================================================//
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //****************************************************************************************//

    @GetMapping("/dashboard/teacher/{id}/update")
    public String viewTeacherToUpdate( Model model , @PathVariable("id") String id){

        Admin admin= adminServiceImp.getAdminById(999);
        model.addAttribute("Admin", admin);

        teacherIdForUpdate = id;
//        System.out.println("Path variable: "+ id);

        Teacher viewInformation = adminServiceImp.getTeacherById(id);



        System.out.println("Teacher Information"+viewInformation);
        model.addAttribute("teacherUpdate", viewInformation);
//        just add only
        model.addAttribute("checkUsername",Integer.parseInt("1"));

        return "teacher_profile";
    }



    @PostMapping("/dashboard/teacher/{id}/updating")
    public String updateTeacher(@ModelAttribute Teacher teacher,
                                @PathVariable String id, Model model
                                ){
        Admin admin= adminServiceImp.getAdminById(999);
        model.addAttribute("Admin", admin);

//        System.out.println("Teacher attribute: "+ teacher);
//        System.out.println("id"+teacherIdForUpdate);
        Teacher updateTeacher = new Teacher();
        Teacher oldTeacher = adminServiceImp.getTeacherById(id);
        updateTeacher.setId(id);
        updateTeacher.setFullName(teacher.getFullName());
        updateTeacher.setEmail(teacher.getEmail());
        updateTeacher.setUsername(teacher.getUsername());
        updateTeacher.setGender(teacher.getGender());

//        System.out.println("test: "+oldTeacher.getUsername().compareTo(updateTeacher.getUsername()));

//       --------when username not update------------
        if(oldTeacher.getUsername().compareTo(updateTeacher.getUsername())==0){
//            System.out.println("same username");
            adminServiceImp.updateTeacher(updateTeacher, id);
            return "redirect:/dashboard/teacher/"+id+"/update";
        }

        boolean hasTeacherUsername = adminServiceImp.checkTeacherUsername(updateTeacher.getUsername());
        if(hasTeacherUsername){
            model.addAttribute("checkUsername",Integer.parseInt("2"));
            model.addAttribute("teacherUpdate",updateTeacher);
            return "teacher_profile";
        }else {
            adminServiceImp.updateTeacher(updateTeacher, id);
            return "redirect:/dashboard/teacher/"+teacherIdForUpdate+"/update";
        }

    }



}
