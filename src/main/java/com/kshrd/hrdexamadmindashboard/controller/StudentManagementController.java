package com.kshrd.hrdexamadmindashboard.controller;

import com.kshrd.hrdexamadmindashboard.model.Admin;
import com.kshrd.hrdexamadmindashboard.model.Generation;
import com.kshrd.hrdexamadmindashboard.model.Student;
import com.kshrd.hrdexamadmindashboard.model.StudentClass;
import com.kshrd.hrdexamadmindashboard.service.AdminServiceImp;
import com.kshrd.hrdexamadmindashboard.utility.Paging;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Controller
public class StudentManagementController {

    @Autowired
    private AdminServiceImp adminServiceImp;
    Paging paging =new Paging();


    @Value("${file.storage.client.path}")
//    @Value("${file.storage.server.path}")
    String serverPath ;
    //========================================================================================//
    //                                                                                        //
    //========================================================================================//
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //****************************************************************************************//
    @GetMapping(value = {"/dashboard/student/management", "/dashboard/student/management/{classId}"})
    public String getAllStudent(Model model, @PathVariable Optional<Integer> classId, @RequestParam Optional<String> search) {
        Admin admin= adminServiceImp.getAdminById(999);

        Generation activeGeneration = adminServiceImp.getGenerationByStatus(1);

        if (classId.isPresent()) {
            if (search.isPresent()) {
                String searching = search.get();
                System.out.println("string search: " + searching);
                List<Student> studentsByIdOrName = adminServiceImp.findStudentsByIdOrNameWithGenID("%" + searching + "%",activeGeneration.getId());
                System.out.println("student result search : " + studentsByIdOrName);
                model.addAttribute("Students", studentsByIdOrName);
                model.addAttribute("textSearch", search.get());
                model.addAttribute("indexClass", ""); // when user search is not select the class.
            } else {
                List<Student> studentsByClass = adminServiceImp.getStudentsByGenerationAndClass(classId.get(), activeGeneration.getId());
                model.addAttribute("Students", studentsByClass);
                model.addAttribute("indexClass", classId.get());
            }
        } else {
            if (search.isPresent()) {
                String searching = search.get();
                System.out.println("string search: " + searching);
                List<Student> studentsByIdOrName = adminServiceImp.findStudentsByIdOrNameWithGenID("%" + searching + "%",activeGeneration.getId());
                System.out.println("student result search : " + studentsByIdOrName);
                model.addAttribute("Students", studentsByIdOrName);
                model.addAttribute("textSearch", search.get());
            } else {
                List<Student> students = adminServiceImp.getStudentsByGeneration(activeGeneration.getId());

                model.addAttribute("Students", students);
            }
        }
        List<Student> classes = adminServiceImp.getAvailableClassByStudent(activeGeneration.getId());
//        System.out.println("Class room : " + classes);
        model.addAttribute("allClassRoom", classes);
        model.addAttribute("genStatus", activeGeneration.getStatus());
        model.addAttribute("genIdStatus",activeGeneration.getId());
        model.addAttribute("Admin", admin);

        return "student_management";
    }

//    ================================ fetch pagination ===========================================
    @GetMapping("/dashboard/student")
    public String fetchAllStudentBySearch(Model model, @RequestParam(required = false) String page, @RequestParam(required = false) String search){
        Admin admin= adminServiceImp.getAdminById(999);

        paging.setLimit(10);
        if(page==null){
            paging.setPage(1);
        }else{
            paging.setPage(Integer.parseInt(page));
        }
        Generation activeGeneration = adminServiceImp.getGenerationByStatus(1);

//        System.out.println(search);
//        System.out.println("page" + paging.getPage());
//        System.out.println("offset"+ paging.getOffset());
//        System.out.println("limit"+ paging.getLimit());
//        System.out.println("total page"+paging.getTotalPages());
        if(search==null){
            System.out.println("not search");
            paging.setTotalCount(adminServiceImp.countStudentsByGeneration(activeGeneration.getId()));
            List<Student> students = adminServiceImp.fetchStudentsByGeneration(activeGeneration.getId(),paging);
            model.addAttribute("Students", students);
        }else {
            System.out.println("have search string");
            paging.setTotalCount(adminServiceImp.countSearchStudents("%" + search + "%",activeGeneration.getId()));
            List<Student> students = adminServiceImp.searchStudentsByIdOrNameWithGenID("%" + search + "%",activeGeneration.getId(),paging);
            model.addAttribute("Students", students);
        }

//        System.out.println(students);

        List<Student> classes = adminServiceImp.getAvailableClassByStudent(activeGeneration.getId());
//        System.out.println("Class room : " + classes);
        model.addAttribute("allClassRoom", classes);
        model.addAttribute("paging",paging);
        model.addAttribute("textSearch", search);
        model.addAttribute("genStatus", activeGeneration.getStatus());
        model.addAttribute("genIdStatus",activeGeneration.getId());
        model.addAttribute("Admin", admin);

        return "student_management";
    }

    @GetMapping("/dashboard/student/{classId}")
    public String fetchAllStudentByClass(Model model,@PathVariable(required = false) String classId,
                                          @RequestParam(required = false) String page, @RequestParam(required = false) String search){
        Admin admin= adminServiceImp.getAdminById(999);

        paging.setLimit(10);

        if(page==null){
            paging.setPage(1);
        }else{
            paging.setPage(Integer.parseInt(page));
        }
        Generation activeGeneration = adminServiceImp.getGenerationByStatus(1);
//        System.out.println(search);
        System.out.println("classID"+classId);
//        System.out.println("page" + paging.getPage());
//        System.out.println("offset"+ paging.getOffset());
//        System.out.println("limit"+ paging.getLimit());
//        System.out.println("total page"+paging.getTotalPages());
        if((search==null)||(search.equals(""))){
            paging.setTotalCount(adminServiceImp.countStudentsByGenerationAndClass(Integer.parseInt(classId),activeGeneration.getId()));
            List<Student> students = adminServiceImp.fetchStudentsByGenerationAndClass(Integer.parseInt(classId),activeGeneration.getId(),paging);
            model.addAttribute("Students", students);
//            System.out.println("search null");
        }else {
            paging.setTotalCount(adminServiceImp.countSearchStudentsByGenerationAndClass("%" + search + "%",
                    Integer.parseInt(classId),activeGeneration.getId()));
            List<Student> students = adminServiceImp.searchStudentsByIdOrNameWithGenIdAndClassId("%" + search + "%",
                   Integer.parseInt(classId),activeGeneration.getId(),paging);
            model.addAttribute("Students", students);
//            System.out.println("search have"+ search);
        }

        List<Student> classes = adminServiceImp.getAvailableClassByStudent(activeGeneration.getId());
//        System.out.println("Class room : " + classes);
        model.addAttribute("allClassRoom", classes);
        model.addAttribute("paging",paging);
        model.addAttribute("textSearch", search);
        model.addAttribute("genStatus", activeGeneration.getStatus());
        model.addAttribute("genIdStatus",activeGeneration.getId());
        model.addAttribute("indexClass", Integer.parseInt(classId));
        model.addAttribute("Admin", admin);
        return "student_management";
    }
    //========================================================================================//
    //                                                                                        //
    //========================================================================================//
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //****************************************************************************************//

    @GetMapping("/dashboard/student/management/filter")
    public String studentManagement(Model model,
                                    @RequestParam("classId") int classId) {

        Admin admin= adminServiceImp.getAdminById(999);
//        System.out.println("class ID"+classId);
        Generation activeGeneration = adminServiceImp.getGenerationByStatus(1);
        List<Student> studentsByClass = adminServiceImp.getStudentsByGenerationAndClass(classId, activeGeneration.getId());
        List<StudentClass> classes = adminServiceImp.getAllClass();

        System.out.println(studentsByClass);
        model.addAttribute("Students", studentsByClass);
        model.addAttribute("allClassRoom", classes);
        model.addAttribute("indexClass", classId);
        model.addAttribute("Admin", admin);
        return "student_management";
    }


    //========================================================================================//
    //                                                                                        //
    //========================================================================================//
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //****************************************************************************************//

//    @GetMapping("/dashboard/student/management/")
//    public String searchStudentByIdOrName(Model model,
//                                      @RequestParam String search){
//
//        List<Student> studentsByIdOrName = adminServiceImp.findStudentsByIdOrName("%"+search+"%");
////        System.out.println("student result search : "+ studentsByIdOrName);
//
//        model.addAttribute("textSearch", search);
//        model.addAttribute("Students", studentsByIdOrName);
//
//        return "student_management";
//    }


    //========================================================================================//
    //                                                                                        //
    //========================================================================================//
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //****************************************************************************************//

    @GetMapping("/dashboard/student/management/{studentId}/delete")
    public String deleteStudentById(@PathVariable String studentId) {

        System.out.println("student id for Delete " + studentId);
        adminServiceImp.deleteStudents(studentId);

        return "redirect:/dashboard/student/management";
    }

    //========================================================================================//
    //                      Update student                                                    //
    //========================================================================================//
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //****************************************************************************************//
    @GetMapping("/dashboard/student/{id}/update")
    public String viewStudentToUpdate(@PathVariable String id, Model model) {
        Admin admin= adminServiceImp.getAdminById(999);

//        Generation activeGen = adminServiceImp.getGenerationByStatus(1);
        Student student = adminServiceImp.getStudentsById(id);
        List<StudentClass> classes = adminServiceImp.getAllClass();
        System.out.println("student for update: " + student);

//        model.addAttribute("generation",activeGen);
        model.addAttribute("student", student);
        model.addAttribute("classrooms", classes);
        model.addAttribute("Admin", admin);
        return "student_profile";
    }

    @PostMapping("/dashboard/student/{id}/updating")
    public String updateStudent(@PathVariable String id,
                                @ModelAttribute("studentUpdate") Student student, @RequestPart("file") MultipartFile file) {
        Admin admin= adminServiceImp.getAdminById(999);

        String savedFileName;

        Student update = new Student();

        update.setFullName(student.getFullName());
        update.setId(student.getId());
        update.setEmail(student.getEmail());
        update.setGender(student.getGender());
        update.setClassroom(student.getClassroom());

        if(!file.isEmpty()){
            String orginalFileName =  file.getOriginalFilename();
            savedFileName = UUID.randomUUID().toString()+"-"+orginalFileName;
            try{

                Files.copy(file.getInputStream(), Paths.get(serverPath,savedFileName));
                System.out.println(Paths.get(serverPath,savedFileName));

                update.setProfileImg(Paths.get(serverPath,savedFileName).toString());

            }  catch (IOException e){
                e.printStackTrace();
            }
        }

//        System.out.println("old ID student for update:"+ id);
        System.out.println("Student Update: "+update);
        adminServiceImp.updateStudent(update, id);
//        ========= assign new id to replace old id
        String newId = student.getId();

        return "redirect:/dashboard/student/" + newId + "/update";
    }

}
