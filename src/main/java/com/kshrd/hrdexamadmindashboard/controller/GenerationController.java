package com.kshrd.hrdexamadmindashboard.controller;

import com.kshrd.hrdexamadmindashboard.model.*;
import com.kshrd.hrdexamadmindashboard.service.AdminServiceImp;
import com.kshrd.hrdexamadmindashboard.utility.Paging;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@Controller
public class GenerationController {
    @Autowired
    private AdminServiceImp adminServiceImp;

Paging paging = new Paging();
    //========================================================================================//
    //                                                                                        //
    //========================================================================================//
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //****************************************************************************************//
    @GetMapping("/dashboard/generation")
    public String generationManagement(Model model, @RequestParam(required = false) String page) {
        Admin admin= adminServiceImp.getAdminById(999);

        Generation activeGeneration = adminServiceImp.getGenerationByStatus(1);

        paging.setLimit(12);
        if(page==null){
            paging.setPage(1);
        }else{
            paging.setPage(Integer.parseInt(page));
        }

        paging.setTotalCount(adminServiceImp.countAllGeneration());
        List<Generation> generations = adminServiceImp.fetchAllGeneration(paging);

//        System.out.println("all gen :"+generations);
        model.addAttribute("activeGeneration",activeGeneration);
        model.addAttribute("generations", generations);
        model.addAttribute("paging",paging);
        model.addAttribute("Admin", admin);
        return "generation_management";
    }


    //========================================================================================//
    //                                                                                        //
    //========================================================================================//
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //****************************************************************************************//

    @PostMapping("/dashboard/generation/add")
    public String createGeneration(@ModelAttribute("generation") Generation generation) {

        Generation activeGeneration = adminServiceImp.getGenerationByStatus(1);

        System.out.println("generation for create: " + generation);

        if (generation.getStatus() == 1) {
            activeGeneration.setStatus(0);
            adminServiceImp.updateGenerations(activeGeneration, activeGeneration.getId());
        }

        adminServiceImp.createGeneration(generation);

        return "redirect:/dashboard/generation";
    }


    //========================================================================================//
    //                                                                                        //
    //========================================================================================//
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //****************************************************************************************//

    @GetMapping("/dashboard/generation/{id}/view")
    public String updateGeneration(Model model, @PathVariable int id) {
        Admin admin= adminServiceImp.getAdminById(999);

        Generation activeGeneration = adminServiceImp.getGenerationByStatus(1);
        Generation getGeneration = adminServiceImp.getGenerationByID(id);
        List<Student> students = adminServiceImp.getStudentsByGeneration(id);
        List<Exam> exams = adminServiceImp.getAllExamByGenId(id);
        int totalStuGen = students.size();
        int totalClassWork = exams.size();

//        System.out.println("exam : "+exams);
//        System.out.println("gen : "+getGeneration);
        model.addAttribute("activeGeneration", activeGeneration);
        model.addAttribute("generation", getGeneration);
        model.addAttribute("totalStudentGen", totalStuGen);
        model.addAttribute("totalClassWork", totalClassWork);
        model.addAttribute("Admin", admin);
        return "view_generation";
    }
    //========================================================================================//
    //                               Manage Students by Generations                           //
    //========================================================================================//
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //****************************************************************************************//

//    @GetMapping(value = {"/dashboard/generation/{id}/view/student", "/dashboard/generation/{id}/view/student/{classId}"})
//    public String viewStudentByGeneration(Model model, @PathVariable int id, @PathVariable Optional<Integer> classId, @RequestParam Optional<String> search) {
//
//        Generation generation = adminServiceImp.getGenerationByID(id);
//
//        if (classId.isPresent()) {
//            if (search.isPresent()) {
//                String searching = search.get();
//                System.out.println("string search: " + searching);
//                List<Student> studentsByIdOrName = adminServiceImp.findStudentsByIdOrNameWithGenID("%" + searching + "%", id);
//                System.out.println("student result search : " + studentsByIdOrName);
//                model.addAttribute("Students", studentsByIdOrName);
//                model.addAttribute("textSearch", search.get());
//                model.addAttribute("indexClass", ""); // when user search is not select the class.
//            } else {
//                List<Student> studentsByClass = adminServiceImp.getStudentsByGenerationAndClass(classId.get(), id);
//                model.addAttribute("Students", studentsByClass);
//                model.addAttribute("indexClass", classId.get());
//            }
//        } else {
//            if (search.isPresent()) {
//                String searching = search.get();
//                System.out.println("string search: " + searching);
//                List<Student> studentsByIdOrName = adminServiceImp.findStudentsByIdOrNameWithGenID("%" + searching + "%", id);
//                System.out.println("student result search : " + studentsByIdOrName);
//                model.addAttribute("Students", studentsByIdOrName);
//                model.addAttribute("textSearch", search.get());
//            } else {
//                List<Student> students = adminServiceImp.getStudentsByGeneration(id);
//                model.addAttribute("Students", students);
//            }
//        }
//        List<StudentClass> classes = adminServiceImp.getAllClass();
////        System.out.println("Class room : " + classes);
//        model.addAttribute("paging",paging);
//        model.addAttribute("allClassRoom", classes);
//        model.addAttribute("genStatus", generation.getStatus());
//        model.addAttribute("genIdStatus", generation.getId());
//        return "student_management";
//    }
//
    //    ============================ fetch student by archived gen whit pagination ===================================
    @GetMapping("/dashboard/generation/{id}/view/student")
    public String fetchArchivedStudentBySearch(Model model,@PathVariable int id, @RequestParam(required = false) String page, @RequestParam(required = false) String search){
        Admin admin= adminServiceImp.getAdminById(999);

        Generation generation = adminServiceImp.getGenerationByID(id);

        if(page==null){
            paging.setPage(1);
        }else{
            paging.setPage(Integer.parseInt(page));
        }

//        System.out.println(search);
//        System.out.println("page" + paging.getPage());
//        System.out.println("offset"+ paging.getOffset());
//        System.out.println("limit"+ paging.getLimit());
//        System.out.println("total page"+paging.getTotalPages());
        if(search==null){
            System.out.println("not search");
            paging.setTotalCount(adminServiceImp.countStudentsByGeneration(generation.getId()));
            List<Student> students = adminServiceImp.fetchStudentsByGeneration(generation.getId(),paging);
            model.addAttribute("Students", students);
        }else {
            System.out.println("have search string");
            paging.setTotalCount(adminServiceImp.countSearchStudents("%" + search + "%",generation.getId()));
            List<Student> students = adminServiceImp.searchStudentsByIdOrNameWithGenID("%" + search + "%",generation.getId(),paging);
            model.addAttribute("Students", students);
        }

//        System.out.println(students);

        List<Student> classes = adminServiceImp.getAvailableClassByStudent(generation.getId());
//        System.out.println("Class room : " + classes);
        model.addAttribute("allClassRoom", classes);
        model.addAttribute("paging",paging);
        model.addAttribute("textSearch", search);
        model.addAttribute("genStatus", generation.getStatus());
        model.addAttribute("genIdStatus",generation.getId());
        model.addAttribute("Admin", admin);

        return "student_management";
    }

    @GetMapping("/dashboard/generation/{id}/view/student/{classId}")
    public String fetchArchivedStudentByClass(Model model, @PathVariable int id, @PathVariable(required = false) String classId,
                                         @RequestParam(required = false) String page, @RequestParam(required = false) String search){
        Admin admin= adminServiceImp.getAdminById(999);

        Generation generation = adminServiceImp.getGenerationByID(id);

        if(page==null){
            paging.setPage(1);
        }else{
            paging.setPage(Integer.parseInt(page));
        }
//        System.out.println(search);
//        System.out.println("classID"+classId);
//        System.out.println("page" + paging.getPage());
//        System.out.println("offset"+ paging.getOffset());
//        System.out.println("limit"+ paging.getLimit());
//        System.out.println("total page"+paging.getTotalPages());
        if((search==null)||(search.equals(""))){
            paging.setTotalCount(adminServiceImp.countStudentsByGenerationAndClass(Integer.parseInt(classId),generation.getId()));
            List<Student> students = adminServiceImp.fetchStudentsByGenerationAndClass(Integer.parseInt(classId),generation.getId(),paging);
            model.addAttribute("Students", students);
//            System.out.println("search null");
        }else {
            paging.setTotalCount(adminServiceImp.countSearchStudentsByGenerationAndClass("%" + search + "%",
                    Integer.parseInt(classId),generation.getId()));
            List<Student> students = adminServiceImp.searchStudentsByIdOrNameWithGenIdAndClassId("%" + search + "%",
                    Integer.parseInt(classId),generation.getId(),paging);
            model.addAttribute("Students", students);
//            System.out.println("search have"+ search);
        }

        List<Student> classes = adminServiceImp.getAvailableClassByStudent(generation.getId());
//        System.out.println("Class room : " + classes);
        model.addAttribute("allClassRoom", classes);
        model.addAttribute("paging",paging);
        model.addAttribute("textSearch", search);
        model.addAttribute("genStatus", generation.getStatus());
        model.addAttribute("genIdStatus",generation.getId());
        model.addAttribute("indexClass", Integer.parseInt(classId));
        model.addAttribute("Admin", admin);
        return "student_management";
    }
    //========================================================================================//
    //                                                                                        //
    //========================================================================================//
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //****************************************************************************************//

    @GetMapping("/dashboard/generation/{id}/delete")
    public String deleteGeneration(@PathVariable int id) {

//        System.out.println("id gen for delete :"+id);
        Generation generation = adminServiceImp.getGenerationByID(id);

        if (generation.getStatus() == 1) {
            return "redirect:/dashboard/generation/" + id + "/view";
        } else {
            adminServiceImp.deleteGeneration(id);
            return "redirect:/dashboard/generation";
        }
    }


    //========================================================================================//
    //                                                                                        //
    //========================================================================================//
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //****************************************************************************************//
    @PostMapping("/dashboard/generation/{id}/edit")
    public String editGeneration(@PathVariable int id, @ModelAttribute Generation newGeneration) {

        System.out.println("generation for update: " + newGeneration);
//        System.out.println("id gen for update :" +id);

        Generation activeGeneration = adminServiceImp.getGenerationByStatus(1);
//        System.out.println("active gen: "+activeGeneration);

        if (newGeneration.getId() == activeGeneration.getId()) {
            // set status to progress this generation because it's the progressed generation
            // newGeneration get from html is 0 because I use attribute "disable" on select option
            // generation must progress only one
            newGeneration.setStatus(1);
            adminServiceImp.updateGenerations(newGeneration, id);

        } else {
            if (newGeneration.getStatus() == 0) {
                adminServiceImp.updateGenerations(newGeneration, id);
            } else {
                activeGeneration.setStatus(0);
                adminServiceImp.updateGenerations(activeGeneration, activeGeneration.getId());
                adminServiceImp.updateGenerations(newGeneration, id);
            }
        }

        return "redirect:/dashboard/generation";
    }
}
