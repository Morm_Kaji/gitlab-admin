package com.kshrd.hrdexamadmindashboard.controller;


import com.kshrd.hrdexamadmindashboard.model.Admin;
import com.kshrd.hrdexamadmindashboard.model.AdminDto;
import com.kshrd.hrdexamadmindashboard.model.AdminLoginRequest;
import com.kshrd.hrdexamadmindashboard.model.Generation;
import com.kshrd.hrdexamadmindashboard.security.jwt.JwtUtil;
import com.kshrd.hrdexamadmindashboard.service.AdminServiceImp;
import com.kshrd.hrdexamadmindashboard.service.MyUserDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Optional;


@Controller
public class AdminController {

    String adminUsername = "";
    int errorChangePassword;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private MyUserDetailService userDetailService;

    @Autowired
    private JwtUtil jwtUtil;

    @Autowired
    private AdminServiceImp adminServiceImp;

    @Autowired
    private PasswordEncoder passwordEncoder;

//    public static String getCurrentUser(){
//        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//        String username;
//        if (principal instanceof UserDetails) {
//            username = ((UserDetails)principal).getUsername();
//        } else {
//            username = principal.toString();
//        }
//        return username;
//    }

    @GetMapping("/login")
    public String home2(){
        return "login_form";
    }

    @GetMapping("")
    public String home(){
        return "login_form";
    }

    @PostMapping( "/login")
    public String login(@ModelAttribute AdminLoginRequest adminLoginRequest, Model model) throws Exception {

        try {
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(adminLoginRequest.getUsername(), adminLoginRequest.getPassword()));
        } catch (BadCredentialsException e) {
            throw new Exception("Incorrect username password", e);
        }

        final UserDetails userDetails = userDetailService
                .loadUserByUsername(adminLoginRequest.getUsername());

        final String jwt = jwtUtil.generateToken(userDetails);

        System.out.println("Token : " + jwt);

        Admin admin = adminServiceImp.getAdminByUsername(adminLoginRequest.getUsername());

        adminUsername = admin.getUsername();

        model.addAttribute("Admin", admin);

        return "redirect:/dashboard";
    }


    //========================================================================================//
    //                                                                                        //
    //========================================================================================//
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //****************************************************************************************//

    @GetMapping("/dashboard")
    public String dashboard(Model model){

        Admin admin = adminServiceImp.getAdminByUsername("sokpolen");
        Generation generation = adminServiceImp.getGenerationByStatus(1);
        int totalStudent = adminServiceImp.getStudentsByGeneration(generation.getId()).size();
        int totalTeacher = adminServiceImp.getAllTeachers().size();
        int totalGeneration = adminServiceImp.getAllGeneration().size();
        int totalClass = adminServiceImp.getAllClass().size();

        model.addAttribute("Admin", admin);
        model.addAttribute("totalStudents", totalStudent);
        model.addAttribute("totalTeacher", totalTeacher);
        model.addAttribute("totalClass",totalClass);
        model.addAttribute("totalGeneration", totalGeneration);

        return "dashboard";
    }


    //========================================================================================//
    //                                                                                        //
    //========================================================================================//
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //****************************************************************************************//
    @GetMapping("/profile/view")
    public String showProfile(Model model){

        Admin admin  = adminServiceImp.getAdminById(999);
        System.out.println("Admin :" +admin);

        errorChangePassword =1;

        model.addAttribute("errorChangePassword",errorChangePassword);
        model.addAttribute("Admin", admin);
        return "admin_profile";
    }
    //========================================================================================//
    //                                                                                        //
    //========================================================================================//
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //****************************************************************************************//

    @PostMapping("/profile/{id}/edit")
    public String adminEdit(@ModelAttribute("admin") Admin admin,
                            @PathVariable int id) {

//      String profileName = adminServiceImp.profile(profileImg);
        Admin profile = new Admin();

        profile.setFullName(admin.getFullName());
        profile.setUsername(admin.getUsername());
        profile.setEmail(admin.getEmail());
        profile.setGender(admin.getGender());
       profile.setProfileImg(admin.getProfileImg());


        System.out.println(admin.getProfileImg());
//        System.out.println("Id admin for update: "+id);
//        System.out.println("admin for update: "+profile);

        adminServiceImp.updateAdminById(profile,id);


        return "redirect:/profile/view";
    }


    @PostMapping("/profile/{id}/changePassword")
    public String changePassword(@ModelAttribute("admin") AdminDto adminDto,
                                 @PathVariable int id, Model model){

        Admin newAdmin = new Admin();
        Admin admin= adminServiceImp.getAdminById(999);

        if(passwordEncoder.matches(adminDto.getPassword(),admin.getPassword())){
            newAdmin.setPassword(passwordEncoder.encode(adminDto.getNewPassword()));
            adminServiceImp.changePasswordAdmin(newAdmin,id);
//            System.out.println("OK");
            errorChangePassword = 2;
            model.addAttribute("Admin", admin);
            model.addAttribute("errorChangePassword", errorChangePassword);
            return "admin_profile";
        }else {
//            System.out.println("WRONG");
            errorChangePassword = 3;
            model.addAttribute("Admin", admin);
            model.addAttribute("errorChangePassword", errorChangePassword);
            return "admin_profile";
        }

    }
    @GetMapping("/setnewpassword")
    public String setNewPassword(){
        return "reset_password";
    }



}
