package com.kshrd.hrdexamadmindashboard.controller;

import com.kshrd.hrdexamadmindashboard.model.Admin;
import com.kshrd.hrdexamadmindashboard.model.Student;
import com.kshrd.hrdexamadmindashboard.model.StudentClass;
import com.kshrd.hrdexamadmindashboard.service.AdminServiceImp;
import com.kshrd.hrdexamadmindashboard.utility.Paging;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.parameters.P;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class ClassController {

    @Autowired
    private AdminServiceImp adminServiceImp;

    Paging paging =new Paging();

    //========================================================================================//
    //                                                                                        //
    //========================================================================================//
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //****************************************************************************************//

    @PostMapping("/dashboard/classroom/add")
    public String createClassroom(@ModelAttribute StudentClass studentClass){

        System.out.println("classroom: "+studentClass);

        adminServiceImp.createClass(studentClass.getClassName());

        return "redirect:/dashboard/classroom/management";
    }


//    ============ Get All Classroom =====================
    @GetMapping("/dashboard/classroom/management")
    public String getAlStudentClass(Model model, @RequestParam(required = false) String page){
        Admin admin= adminServiceImp.getAdminById(999);

        paging.setLimit(12);

        if(page==null){
            paging.setPage(1);
        }else{
            paging.setPage(Integer.parseInt(page));
        }

        paging.setTotalCount(adminServiceImp.countAllClass());
        List<StudentClass> studentClasses = adminServiceImp.fetchAllClass(paging);
        System.out.println("class room :"+studentClasses);


        model.addAttribute("Classrooms", studentClasses);
        model.addAttribute("paging", paging);
        model.addAttribute("Admin", admin);
        return "class_management";
    }

//    ================= update class ========================
    @PostMapping("/dashboard/classroom/{id}/edit")
    public String updateClassroom(@PathVariable int id, @ModelAttribute StudentClass classroom ){

//        System.out.println("class for update: "+ classroom);
//        System.out.println("id class: "+id);

        adminServiceImp.updateClass(classroom,id);

        return "redirect:/dashboard/classroom/management";
    }

//    ============== delete class ==============
    @GetMapping("/dashboard/classroom/{id}/delete")
    public String deleteClassroom(@PathVariable int id){

        System.out.println("id for delete : "+ id);

        adminServiceImp.deleteClass(id);

        return "redirect:/dashboard/classroom/management";
    }
}
