package com.kshrd.hrdexamadmindashboard.repository.dataProvider;

import com.kshrd.hrdexamadmindashboard.utility.Paging;
import org.apache.ibatis.annotations.One;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.jdbc.SQL;

public class DataProvider {

    public String fetchAllStudent(@Param("genId") int genId, @Param("classId") int classId, Paging paging,
                                  @Param("search") String search){

        return new SQL(){{
            SELECT ("id, full_name, gender, email , password, profile_image, generation_id, class_id");
             FROM( " FROM students ");
            WHERE(" generation_id = #{genId}");
        }}.toString();
    }

}
