package com.kshrd.hrdexamadmindashboard.repository;


import com.kshrd.hrdexamadmindashboard.model.*;
import com.kshrd.hrdexamadmindashboard.utility.Paging;
import org.apache.ibatis.annotations.*;
import org.springframework.security.core.parameters.P;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AdminRepository {
    //============================================== Admin ================================================================
    @Select("SELECT id, full_name, gender, email, username, password, profile_image FROM admin WHERE username=#{username}")
    @Result(property = "fullName", column = "full_name")
    @Result(property = "profileImg", column = "profile_image")
    Admin getAdminByUsername(String username);

    @Select("SELECT id, full_name, gender, email, username, password, profile_image FROM admin WHERE id=#{id}")
    @Result(property = "fullName", column = "full_name")
    @Result(property = "profileImg", column = "profile_image")
    Admin getAdminById(int id);

    @Update("UPDATE admin SET full_name = #{admin.fullName}," +
            "gender = #{admin.gender}," +
            "email = #{admin.email}," +
            "username = #{admin.username}," +
            "profile_image = #{admin.profileImg}" +
            "WHERE id = #{adminId}")
    Boolean updateAdminById(Admin admin, int adminId);

    @Update("UPDATE admin SET password = #{admin.password}" +
            " WHERE id = #{adminId}")
    Boolean changePasswordAdmin(Admin admin, int adminId);
//====================================== Teacher =======================================================================

    @Insert("INSERT INTO teachers(id, full_name, gender, email, username, password, profile_image, status) " +
            " VALUES (#{teacher.id}, #{teacher.fullName}, #{teacher.gender}, #{teacher.email}, #{teacher.username}, #{teacher.password}, #{teacher.profileImg}, #{teacher.status})")
    void registerTeacher(@Param("teacher") Teacher teacher);

    @Select("SELECT id, full_name, gender, email, username, password, profile_image, status" +
            " FROM teachers WHERE status=1 ORDER BY full_name")
    @Result(property = "fullName", column = "full_name")
    @Result(property = "profileImg", column = "profile_image")
    List<Teacher> getAllTeachers();

    @Select("SELECT id, full_name, gender, email, username, password, profile_image, status" +
            " FROM teachers WHERE id = #{teacher}")
    @Result(property = "fullName", column = "full_name")
    @Result(property = "profileImg", column = "profile_image")
    Teacher getTeachersById(String teacher);

    @Update("UPDATE teachers SET full_name = #{teacher.fullName}," +
            "username = #{teacher.username}," +
            "email = #{teacher.email}," +
            "gender = #{teacher.gender}" +
//             will add profile image
            " WHERE id = #{id}")
    void updateTeacher(@Param("teacher") Teacher teacher, String id);

//    ----------- Delete Form Table---------------
//    @Delete("DELETE FROM teachers WHERE id = #{teacher}")
//    void deleteTeacher(String teacher);

    //    ------------ Delete by Change status -------------------
    @Update("UPDATE teachers SET status=0 WHERE id = #{teacher}")
    void deleteTeacherById(String teacher);

    //   --------------- search teacher by name or id ----------------
    @Select("SELECT id, full_name, gender, email, username, password, profile_image, status FROM teachers" +
            " WHERE status = 1 AND ((LOWER(id) LIKE LOWER(#{search})) OR (LOWER(full_name) LIKE LOWER(#{search}))" +
            " OR (LOWER(username) LIKE LOWER(#{search})));")
    @Result(property = "fullName", column = "full_name")
    @Result(property = "profileImg", column = "profile_image")
    List<Teacher> findTeacherByIdOrName(String search);

    @Select("SELECT EXISTS (SELECT * FROM teachers WHERE username= #{username})")
    boolean checkTeacherUsername(String username);

    //======================================= Pagination =================================================
//    --------------- fetch all teacher with pagination--------------------------
    @Select("SELECT id, full_name, gender, email, username, password, profile_image, status, ROW_NUMBER() OVER () AS my_index" +
            " FROM teachers WHERE status=1 ORDER BY my_index DESC" +
            " LIMIT #{page.limit} OFFSET #{page.offset}")
    @Result(property = "fullName", column = "full_name")
    @Result(property = "profileImg", column = "profile_image")
    List<Teacher> fetchAllTeachers(@Param("page") Paging paging);

    @Select("SELECT  COUNT(*) FROM teachers WHERE status=1 ")
    int countAllTeacher();

    //   --------------- search teacher by name or id with pagination ----------------
    @Select("SELECT id, full_name, gender, email, username, password, profile_image, status FROM teachers" +
            " WHERE status = 1 AND ((LOWER(email) LIKE LOWER(#{search})) OR (LOWER(full_name) LIKE LOWER(#{search}))" +
            " OR (LOWER(username) LIKE LOWER(#{search})))" +
            " LIMIT #{page.limit} OFFSET #{page.offset}")
    @Result(property = "fullName", column = "full_name")
    @Result(property = "profileImg", column = "profile_image")
    List<Teacher> searchTeacherByIdOrName(String search, @Param("page") Paging paging);

    @Select("SELECT COUNT(*) FROM teachers" +
            " WHERE status = 1 AND ((LOWER(id) LIKE LOWER(#{search})) OR (LOWER(full_name) LIKE LOWER(#{search}))" +
            " OR (LOWER(username) LIKE LOWER(#{search})))")
    int countSearchTeacher(String search);
//    ================================================================================================
// ========================================== Students Management====================================================

    @Select("SELECT id, full_name, gender, email, password, profile_image, generation_id, class_id" +
            " FROM students ORDER BY full_name ")
    @Result(property = "fullName", column = "full_name")
    @Result(property = "profileImg", column = "profile_image")
    @Result(property = "generation", column = "generation_id", one = @One(select = "getGeneration"))
    @Result(property = "classroom", column = "class_id", one = @One(select = "getClassroom"))
    List<Student> getAllStudents();

    @Select("SELECT id, full_name, gender, email, password, profile_image, generation_id, class_id" +
            " FROM students WHERE (LOWER(id) LIKE LOWER(#{search})) OR (LOWER(full_name) LIKE LOWER(#{search})) ")
    @Result(property = "fullName", column = "full_name")
    @Result(property = "profileImg", column = "profile_image")
    @Result(property = "generation", column = "generation_id", one = @One(select = "getGeneration"))
    @Result(property = "classroom", column = "class_id", one = @One(select = "getClassroom"))
    List<Student> findStudentsByIdOrName(String search);

    @Select("SELECT id, full_name, gender, email, password, profile_image, generation_id, class_id" +
            " FROM students WHERE generation_id = #{generationId} AND " +
            " ((LOWER(id) LIKE LOWER(#{search})) OR (LOWER(full_name) LIKE LOWER(#{search}))) ")
    @Result(property = "fullName", column = "full_name")
    @Result(property = "profileImg", column = "profile_image")
    @Result(property = "generation", column = "generation_id", one = @One(select = "getGeneration"))
    @Result(property = "classroom", column = "class_id", one = @One(select = "getClassroom"))
    List<Student> findStudentsByIdOrNameWithGenID(String search, int generationId);

    @Select("SELECT id, full_name, gender, email, password, profile_image, generation_id, class_id" +
            " FROM students WHERE id = #{search}")
    @Result(property = "fullName", column = "full_name")
    @Result(property = "profileImg", column = "profile_image")
    @Result(property = "generation", column = "generation_id", one = @One(select = "getGeneration"))
    @Result(property = "classroom", column = "class_id", one = @One(select = "getClassroom"))
    Student getStudentsById(String search);

    @Select("SELECT id, full_name, gender, email, password, profile_image, generation_id, class_id" +
            " FROM students WHERE class_id = #{classID}")
    @Result(property = "fullName", column = "full_name")
    @Result(property = "profileImg", column = "profile_image")
    @Result(property = "generation", column = "generation_id", one = @One(select = "getGeneration"))
    @Result(property = "classroom", column = "class_id", one = @One(select = "getClassroom"))
    List<Student> getStudentsByClass(int classID);

    @Select("SELECT id, full_name, gender, email, password, profile_image, generation_id, class_id" +
            " FROM students WHERE (class_id = #{classID}) AND (generation_id = #{generationId})")
    @Result(property = "fullName", column = "full_name")
    @Result(property = "profileImg", column = "profile_image")
    @Result(property = "generation", column = "generation_id", one = @One(select = "getGeneration"))
    @Result(property = "classroom", column = "class_id", one = @One(select = "getClassroom"))
    List<Student> getStudentsByGenerationAndClass(int classID, int generationId);

    @Select("SELECT id, full_name, gender, email , password, profile_image, generation_id, class_id" +
            " FROM students WHERE generation_id = #{generationId}")
    @Result(property = "fullName", column = "full_name")
    @Result(property = "profileImg", column = "profile_image")
    @Result(property = "generation", column = "generation_id", one = @One(select = "getGeneration"))
    @Result(property = "classroom", column = "class_id", one = @One(select = "getClassroom"))
    List<Student> getStudentsByGeneration(int generationId);

    //================================ with pagination ===================================
//    --------------- fetch all student ----------------------------
    @Select("SELECT id, full_name, gender, email , password, profile_image, generation_id, class_id, ROW_NUMBER() OVER () AS my_index" +
            " FROM students WHERE generation_id = #{generationId} ORDER BY my_index DESC LIMIT #{page.limit} OFFSET #{page.offset}")
    @Result(property = "fullName", column = "full_name")
    @Result(property = "profileImg", column = "profile_image")
    @Result(property = "generation", column = "generation_id", one = @One(select = "getGeneration"))
    @Result(property = "classroom", column = "class_id", one = @One(select = "getClassroom"))
    List<Student> fetchStudentsByGeneration(int generationId, @Param("page") Paging paging);

    @Select("SELECT COUNT(*) FROM students WHERE generation_id = #{generationId} ")
    int countStudentsByGeneration(int generationId);

    //---------------------fetch student by class with gen--------------------
    @Select("SELECT id, full_name, gender, email, password, profile_image, generation_id, class_id" +
            " FROM students WHERE (class_id = #{classID}) AND (generation_id = #{generationId})" +
            " LIMIT #{page.limit} OFFSET #{page.offset}")
    @Result(property = "fullName", column = "full_name")
    @Result(property = "profileImg", column = "profile_image")
    @Result(property = "generation", column = "generation_id", one = @One(select = "getGeneration"))
    @Result(property = "classroom", column = "class_id", one = @One(select = "getClassroom"))
    List<Student> fetchStudentsByGenerationAndClass(int classID, int generationId, @Param("page") Paging paging);

    @Select("SELECT COUNT(*) FROM students WHERE (class_id = #{classID}) AND (generation_id = #{generationId})")
    int countStudentsByGenerationAndClass(int classID, int generationId);

    //* * * --------------------fetch student by class with gen and class--------------------
    @Select("SELECT id, full_name, gender, email, password, profile_image, generation_id, class_id" +
            " FROM students" +
            " WHERE generation_id = #{generationId} AND (class_id = #{classId}) AND" +
            " ((LOWER(email) LIKE LOWER(#{search})) OR (LOWER(full_name) LIKE LOWER(#{search})))" +
            " LIMIT #{page.limit} OFFSET #{page.offset}")
    @Result(property = "fullName", column = "full_name")
    @Result(property = "profileImg", column = "profile_image")
    @Result(property = "generation", column = "generation_id", one = @One(select = "getGeneration"))
    @Result(property = "classroom", column = "class_id", one = @One(select = "getClassroom"))
    List<Student> searchStudentsByIdOrNameWithGenIdAndClassId(String search, int classId, int generationId, @Param("page") Paging paging);

    @Select("SELECT COUNT(*)" +
            " FROM students WHERE generation_id = #{generationId} AND (class_id = #{classId}) AND" +
            " ((LOWER(id) LIKE LOWER(#{search})) OR (LOWER(full_name) LIKE LOWER(#{search})))")
    int countSearchStudentsByGenerationAndClass(String search, int classId, int generationId);

    //    ----------------- search student----------------
    @Select("SELECT id, full_name, gender, email, password, profile_image, generation_id, class_id" +
            " FROM students WHERE generation_id = #{generationId} AND " +
            " ((LOWER(email) LIKE LOWER(#{search})) OR (LOWER(full_name) LIKE LOWER(#{search})))" +
            " LIMIT #{page.limit} OFFSET #{page.offset}")
    @Result(property = "fullName", column = "full_name")
    @Result(property = "profileImg", column = "profile_image")
    @Result(property = "generation", column = "generation_id", one = @One(select = "getGeneration"))
    @Result(property = "classroom", column = "class_id", one = @One(select = "getClassroom"))
    List<Student> searchStudentsByIdOrNameWithGenID(String search, int generationId, @Param("page") Paging paging);

    @Select("SELECT COUNT(*) FROM students " +
            "WHERE generation_id = #{generationId} AND ((LOWER(id) LIKE LOWER(#{search})) OR (LOWER(full_name) LIKE LOWER(#{search})))")
    int countSearchStudents(String search, int generationId);
//===========================================================================================================

    @Update("UPDATE students SET id = #{student.id}," +
            " full_name = #{student.fullName}," +
            " email = #{student.email}," +
            " gender = #{student.gender}," +
            " class_id = #{student.classroom.id}," +
            " profile_image = #{student.profileImg}"+
            " WHERE id = #{studentId}")
    void updateStudent(Student student, @Param("studentId") String id);

    @Select("SELECT class_name, id FROM class WHERE id=#{class_id}")
    @Result(property = "className", column = "class_name")
    StudentClass getClassroom(@Param("class_id") int id);

    @Select("SELECT id, generation, status, type FROM generations WHERE id=#{generation_id}")
    @Result(property = "generationName", column = "generation")
    Generation getGeneration(@Param("generation_id") int id);

    //    @Update("UPDATE students SET status = 0 WHERE id = #{studentId}")
//    void deleteStudents(String studentId);
    @Delete("DELETE FROM students WHERE id = #{studentId}")
    void deleteStudents(String studentId);

    //    ===========================Class and Generation Management==================================

    @Insert("INSERT INTO class(class_name) VALUES (#{class})")
    void createClass(@Param("class") String className);

    @Select("SELECT id, class_name FROM class")
    @Result(property = "className", column = "class_name")
    List<StudentClass> getAllClass();

    @Select("SELECT DISTINCT ON (class_id) id, full_name, gender, email, password, profile_image, generation_id, class_id" +
            " FROM students WHERE generation_id = #{GenId}")
    @Result(property = "fullName", column = "full_name")
    @Result(property = "profileImg", column = "profile_image")
    @Result(property = "generation", column = "generation_id", one = @One(select = "getGeneration"))
    @Result(property = "classroom", column = "class_id", one = @One(select = "getClassroom"))
    List<Student> getAvailableClassByStudentAndGen(int GenId);

    @Update("UPDATE class SET class_name = #{classStudents.className} WHERE id = #{classId}")
    void updateClass(StudentClass classStudents, @Param("classId") int id);

    @Delete("DELETE FROM class WHERE id = #{classId}")
    void deleteClass(@Param("classId") int id);

    @Insert("INSERT INTO generations(generation, status, type) VALUES (#{generation}, #{status}, #{type})")
    void createGeneration(@Param("generation") int generationName, @Param("status") int status, @Param("type") int type);

    @Select("SELECT id, generation, status, type FROM generations ORDER BY generation DESC, type DESC")
    @Result(property = "generationName", column = "generation")
    List<Generation> getAllGeneration();
//    ========================= pagination ==============================
@Select("SELECT id, generation, status, type FROM generations ORDER BY generation DESC, type DESC" +
        " LIMIT #{page.limit} OFFSET #{page.offset}")
@Result(property = "generationName", column = "generation")
List<Generation> fetchAllGeneration(@Param("page") Paging paging);

@Select("SELECT COUNT(*) FROM generations")
int countAllGeneration();

    @Select("SELECT id, class_name FROM class LIMIT #{page.limit} OFFSET #{page.offset}")
    @Result(property = "className", column = "class_name")
    List<StudentClass> fetchAllClass(@Param("page") Paging paging);

    @Select("SELECT COUNT(*) FROM class")
    int countAllClass();
//    ============================= end pagination repo==============================
    @Select("SELECT id, generation, status, type FROM generations WHERE id = #{generationId}")
    @Result(property = "generationName", column = "generation")
    Generation getGenerationByID(int generationId);

    @Select("SELECT id, generation, status, type FROM generations WHERE status = #{genStatus}")
    @Result(property = "generationName", column = "generation")
    Generation getGenerationByStatus(@Param("genStatus") int status);

    @Update("UPDATE generations SET generation = #{generation.generationName}, status = #{generation.status}, type = #{generation.type} WHERE id = #{id}")
    void updateGenerations(Generation generation, int id);

    @Delete("DELETE FROM generations WHERE id = #{id}")
    void deleteGeneration(int id);

    //================================================   exam    ===========================================================
    @Select("SELECT id, exam_subject, exam_date, exam_name, total_score, teacher_id, duration, generation_id, status, assign_type_id" +
            " FROM exams WHERE generation_id = #{id}")
    @Result(property = "examSubject", column = "exam_subject")
    @Result(property = "examDate", column = "exam_date")
    @Result(property = "examName", column = "exam_name")
    @Result(property = "totalScore", column = "total_score")
    @Result(property = "teacher", column = "teacher_id", one = @One(select = "getTeacherNameByTeacherId"))
    @Result(property = "generation", column = "generation_id", one = @One(select = "getGenNameByGenId"))
    @Result(property = "assignType", column = "assign_type_id")
    List<Exam> getAllExamByGenId(@Param("id") int genId);

    @Select("SELECT full_name FROM teachers WHERE id = #{teacher_id}")
    String getTeacherNameByTeacherId(@Param("teacher_id") String teacherId);

    @Select("SELECT generation FROM generations WHERE id = #{generation_id}")
    int getGenNameByGenId(@Param("generation_id") int genId);
}
