package com.kshrd.hrdexamadmindashboard.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class AdminProfile implements WebMvcConfigurer {

    @Value("@{file.storage.sever.path}")
    private String server;
    @Value("@{file.storage.client.path")
    private String client;

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler(client).addResourceLocations("file:/"+server);
    }
}
