package com.kshrd.hrdexamadmindashboard.config;


import com.kshrd.hrdexamadmindashboard.security.jwt.JwtRequestFilter;
import com.kshrd.hrdexamadmindashboard.service.MyUserDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    private MyUserDetailService myUserDetailService;

    private JwtRequestFilter jwtRequestFilter;

    @Autowired
    public void setMyUserDetailService(MyUserDetailService myUserDetailService) {
        this.myUserDetailService = myUserDetailService;
    }

    @Autowired
    public void setJwtRequestFilter(JwtRequestFilter jwtRequestFilter) {
        this.jwtRequestFilter = jwtRequestFilter;
    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }


    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(myUserDetailService)
                .passwordEncoder(passwordEncoder());
    }


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .authorizeRequests()
                .antMatchers(
                        "/login","/","/profile/view","/favicon.ico","/dashboard/generation/css/modal.css",
                        "/dashboard", "/dashboard/teacher/management","/dashboard/student/{id}/updating","/dashboard/student/{id}/update",
                        "/dashboard/teacher/{id}/update", "/dashboard/teacher/{id}/updating","/dashboard/student/management/**",
                        "/dashboard/student/management/search/{id}","/dashboard/student/management/{studentID}/delete",
                        "/dashboard/student/{id}/update","/dashboard/teacher/management/{id}/delete",
                        "/dashboard/teacher/{id}/view","/dashboard/teacher/register","/dashboard/teacher/register/new",
                        "/dashboard/teacher/{id}/update","/dashboard/generation","/dashboard/student/management",
                        "/dashboard/generation/{id}/view","/profile/{id}/edit","/dashboard/student/management/**",
                        "/dashboard/generation/{id}/view/student","/dashboard/generation/{id}/view/student/**","/dashboard/teacher/management/**",
                        "/dashboard/generation/{id}/view","/dashboard/generation/{id}/edit","/profile/{id}/changePassword","/dashboard/classroom/add",
                        "/setnewpassword","/dashboard/generation/add","/dashboard/generation/{id}/delete","/dashboard/classroom/management",
                        "/dashboard/classroom/{id}/edit","/dashboard/classroom/{id}/delete","/dashboard/student","/dashboard/student/{classId}"
                )
                .permitAll()
//       .antMatchers("/secure/**").hasRole("USER")
                .anyRequest().authenticated()
//                .and()
//                .exceptionHandling().authenticationEntryPoint(jwtAuthenticationEntryPoint)
                .and().sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and().rememberMe().rememberMeParameter("remember");
//   .httpBasic();
        http.addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter.class)
        ;
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring()
                .antMatchers(HttpMethod.OPTIONS, "/**")
                .antMatchers("/resources/**", "/static/**", "/css/**", "/js/**","/fragments/**",
                        "/images/**","/icons/**","/logo/**",
                        "/v3/api-docs/**",
                        "/swagger-ui/**");
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }
}
