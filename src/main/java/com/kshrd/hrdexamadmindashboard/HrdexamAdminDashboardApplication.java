package com.kshrd.hrdexamadmindashboard;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HrdexamAdminDashboardApplication {

    public static void main(String[] args) {
        SpringApplication.run(HrdexamAdminDashboardApplication.class, args);
    }

}
