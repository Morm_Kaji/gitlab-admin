package com.kshrd.hrdexamadmindashboard.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Admin {

    int id;
    String fullName;
    String gender;
    String email;
    String username;
    String password;
    String profileImg;
}
