package com.kshrd.hrdexamadmindashboard.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Generation {

    public int id;
    public int generationName;
    public int status;
    public int type;

}
