package com.kshrd.hrdexamadmindashboard.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Exam {
    String id;
    String examSubject;
    String examDate;
    String examName;
    double totalScore;
    String teacher;
    int duration;
    int generation;
    int status;
    int assignType;
}
