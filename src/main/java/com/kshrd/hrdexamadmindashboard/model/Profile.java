package com.kshrd.hrdexamadmindashboard.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Profile {

    private String fileId;
    private String fileURL;
    private String dateUpload;
}
