package com.kshrd.hrdexamadmindashboard.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@Data
@NoArgsConstructor
public class Teacher {
    public String id;
    public String fullName;
    public String gender;
    public String email;
    public String username;
    public String password;
    public String profileImg;
    public int status;
}
