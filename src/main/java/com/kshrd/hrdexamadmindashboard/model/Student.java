package com.kshrd.hrdexamadmindashboard.model;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class Student {

    public String id;
    public String fullName;
    public String gender;
    public String email;
    public String password;
    public String profileImg;
    public String createAt;
    StudentClass classroom;
    Generation generation;

}
